public class Chessboard {

    private static final int SIZE = 6;
    private char[][] chessboard;
    private static final char BLACK = 'Ч';
    private static final char WHITE = 'Б';

    public Chessboard() {

        chessboard = fillChessboard();
        printChessboard();
    }

    private char[][] fillChessboard() {
        char[][] board = new char[SIZE][SIZE];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                board[i][j] = paintCell(i, j);
            }
        }
        return board;
    }

    private char paintCell(int i, int j) {
        char cell = WHITE;
        if ((i + j) % 2 == 0) {
            cell = BLACK;
        }
        return cell;
    }

    private void printChessboard() {
        for (int i = 0; i < chessboard.length; i++) {
            for (int j = 0; j < chessboard.length; j++) {
                System.out.print(chessboard[i][j] + " ");
            }
            System.out.println();
        }
    }
}